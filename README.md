[[_TOC_]]

# User-Centered Design Introduction:

You've been in a brainstorming session about a new product, service or feature and are now tasked to lay these ideas down and create a design/mockup/wireframe. This project goes over the User Centered Design (or UCD) Framework and Guidelines to achieve this. 

***Note about Usability, UCD, & UX:*** 
As outlined by Travis Lowdermilk in his book, [User-Centered Design](https://learning.oreilly.com/library/view/user-centered-design/9781449359812/index.html)<sup>[Get Paid Access](https://gitlab.com/smarter-codes/guidelines/about-guidelines/-/blob/master/paid-learning-material.md)</sup>, Usability is not UCD. Usability is specifically focused on the interface of a product and whether it is easy to use, easy to learn – all the things listed in [Quesenbery’s 5Es](http://wqusability.com/articles/getting-started.html).

**Usability testing** allows us to construct task-based assessment to evaluate these dimensions of a product.

**User-Centered Design** is a philosophy or approach to the design and development process.

**User Experience** is about the value you are delivering at every point that the user touches a product. This value must align with user and business goals and provide a positive experience on an emotional, social, cultural, psychological and physiological level. In User Experience Design, you find the alignment in the user and business goals and return to these iteratively during the UCD process, all to create the value proposition which will shape the overarching user experience.

Reference: [ - ](https://drive.google.com/file/d/1rreKLE_gXjrZzqhjOZtrB6sYesxhbtGS/view?usp=sharing)



# WHAT is UCD? 

UCD is defined as ‘an iterative design process framework that incorporates validation from the user every step of the way.  It encompasses different research and design tactics CENTERED around the user, thereby leading us to deeply understand the User, the needs and expectations of the User, the major tasks a user will have to perform, and how they’re ultimately going to interact with each part of our product,  the business goals(Stakeholder Vision), and finally the [USP](https://www.entrepreneur.com/encyclopedia/unique-selling-proposition-usp).



# WHY should we use UCD? 

Using UCD we ensure the product to be built/being built is what the user would want to use. It will also help the team in the following: 

- Effective in identifying the UX problems before they occur.
- Validate if our assumptions meet the user’s mental model.
- Identify the critical tasks and prioritize them - narrowing down on USP. 
- Build a positive experience.
- Save money and time of development.

*Note: UCD and Triple Diamond Framework{WIP} of Product Development also complement each other*



# HOW - To apply the UCD Framework and Guidelines:

[Usability.gov](https://www.usability.gov/how-to-and-tools/resources/ucd-map.html) details the UCD framework as a flow chart. 

<img src="https://i.imgur.com/X1FCISY.png" alt="image-20210405105211583" style="zoom:80%;" />


## Step 1 - Plan: 

Begin the process by creating a design strategy or plan which would help to acquire a clear view of the problem. It identifies the problem and derives a good solution considering the goals of the business, user needs, and other critical aspects. 

<img src="https://i.imgur.com/qLANj9B.png" alt="image-20210405105715272" style="zoom:80%;" />

Our Plan comprises of the following:


- Target Users/Market Segment: Primary users who are likely to be using the product. Our design should be able to satisfy the needs of this group. 
- General Tasks: Common set of tasks which would be performed by the target users with the product.
- CSF(Critical success factors): Defines the important outcomes to be met to ensure customer satisfaction.

Deliverables: Statement of Work, Research Plan




## Step 2 - Analyze: 

The Analyze Phase is to understand the characteristics of the target users, analyze the differences among them, identify the critical tasks they would prefer to perform, and know the different user aspects that would influence the design.

<img src="https://i.imgur.com/9Tp34pw.pngg" alt="image-20210405105739670" style="zoom:80%;" />

 Stakeholder Interviews
 Competitive Analysis

### 2.1: Create User Profile through Research(Learn About Users): 

Captures the demographics of the user such as language, age, gender, education, and expertise. User Interviews - using various research methods like  User Interviews

### 2.2 Conduct Task Profile/Task Analysis(Conduct Task Analysis): 

List down all the tasks and specifies which are the different tasks performed by each user. 

### 2.3 Develop Personas:

[Personas](https://www.usability.gov/how-to-and-tools/methods/personas.html) are fictional characters created to represent different user types that would interact with your product. They help to understand the target users in detail by gathering knowledge about their needs, goals, expectations, motivations, and pain points. 

### 2.4 Write Scenarios: 

It is a fictional short story about the user accomplishing a task using the product. It gives a clear idea of who the user would be, why would he/she use your product, what task would they prefer to execute using the product.

### 2.5 Setting Measurable Goals: 

a. **Business Goals**: Specifies what the company wants to achieve over a period of time in terms of branding, sales, profit, etc. 

b. **Marketing/Branding Goals**: Specifies the values/personality the design would possess and offer to its users. It should always focus on establishing a significant presence in the market that attracts and retains customers.

c. **Product Roadmap and Milestones**: Coming up with a Release Plan for MVP

### 2.6: Task Flows: 

It is a flow diagram that shows different steps a user would perform to complete a task. Task flow is different from user flow which shows all possible paths a user can navigate through the application. However, the goal of both is to optimize the user’s ability to accomplish a task without any difficulty.



## Step 3 - Design: 

Once we've learned about our Users and their expectations (+Stakeholder input) we use the Design or Make Phase to create testable designs. 

<img src="https://i.imgur.com/1MleEA3.png" alt="image-20210405105852629" style="zoom:80%;" />


### 3.1 Determine Site/App Requirements: 

Specifies the limitations in implementing the concept using the current technology.

### 3.2 Conduct a Content Inventory:

Content Inventory([Usability.gov](https://www.usability.gov/how-to-and-tools/methods/content-inventory.html)) is used to perform a Content Audit to list down analyze all the content on an existing website (including pages, files, videos, audio or other data) that your users might reasonably encounter. 

### 3.3 Perform Card Sorting: 

Card sorting is a technique where users are asked to generate a folksonomy, or information hierarchy, which can then form the basis of an information architecture or website navigation menu. 

### 3.4 Primary Noun Architecture:

It identifies the key jargons (buzzwords in the market)  that can be used to build the navigation scheme.

### 3.5 Define Information Architecture:

It defines the organization of the product’s(site/application) content and its functionality. The relationship between different jargons captured in the preceding stage will be established using IA.

### 3.6 Write for Web - Real Copy:

Using Real Copy instead of placeholders(lorem ipsum) in prototypes/wireframes. Requires all the copy to be written up - refer UX writing guidelines by [UX Planet](https://uxplanet.org/16-rules-of-effective-ux-writing-2a20cf85fdbf) & [Adobe](https://xd.adobe.com/ideas/process/information-architecture/ux-writing-guidelines/)

### 3.7 Use Parallel Design:

In [Parallel Design](https://www.usability.gov/how-to-and-tools/methods/parallel-design.html#:~:text=With%20the%20parallel%20design%20technique,her%20concepts%20with%20the%20group) multiple designers independently of each other design suggest/design/work on user interfaces for same set of requirements and research. These interfaces are then merged to a unified design or the designers evaluate each others design and adopt the best ideas into their own design. 

### 3.8 Develop a Prototype:

It is the visual guide of the proposed solution that represents the framework of the product. This visual guide can then be used to test and validate our design ideas and assumptions.

***Note on Prototype vs Wireframe***
In a nutshell, prototype is typically medium to high fidelity and functional or static. Wireframes are almost always low fidelity and static. More differences - [Invision](https://www.invisionapp.com/inside-design/wireframe-prototype-difference/)

### 3.9 Beta Launch 

Part of any [software release life cycle](https://en.wikipedia.org/wiki/Software_release_life_cycle), select a small group representative of our prospective User Base and launching the product in an early-stage functioning state, in either an open(public) or closed(private) beta to interested users. With Beta Launches, we can validate the problem/solution fit and can be used to raise product awareness.

Using feedback from Step 4, we iteratively improve the product through to a Rollout(Commercial Availability). 



## Step 4:  

In this phase we validate our hypotheses and concepts through testing. And with retrospection, adopt the feedback to our design, iteratively. 

<img src="https://i.imgur.com/Ih9vDAu.png" alt="image-20210405105916150" style="zoom:80%;" />

### 4.1  Usability Testing:

Usability testing is the practice of testing how easy a design is to use with a group of representative users. "Typically, during a test, participants will try to complete typical tasks while observers watch, listen and takes notes.  The goal is to identify any usability problems, collect qualitative and quantitative data and determine the participant's satisfaction with the product." - [Usability.gov](https://www.usability.gov/how-to-and-tools/methods/usability-testing.html)

Process -

### 4.2 Heuristic Evaluations:

Its a [method](https://www.nngroup.com/articles/how-to-conduct-a-heuristic-evaluation/) for finding the usability problems in a user interface design so that they can be attended to as part of an iterative design process. Heuristic evaluation involves having a small set of evaluators examine the interface and judge its compliance with recognized usability principles([10 Usability Heuristics](https://www.nngroup.com/articles/ten-usability-heuristics/)).  

### 4.3 Implement & Retest: 

Using the feedback from the validation methods we iteratively improve the product through **Build > Measure > Learn > Iterate**, even after Rollout(Commercial Availability)

# Inspirations
* [Product & Design blog by Intercom.io](https://www.intercom.com/blog/product-and-design/)
* [Usability.gov](https://usability.gov/)
